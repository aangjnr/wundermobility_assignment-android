# Wunder Mobility Android Assignment Repo

### Code Stack

* Language - **Java**
* Minimum SDK - **16**
* Target SDK - **28**
* Architecture Model View Presenter
* Asynchronous operations - **RxJava/RxAndroid**
* Network Calls - **Retrofit + OkHttp**
* Tests - **JUnit/Espresso/Mockito**


### Why MVP?

* MVP architecture was purposely used for this project because of its simplicity, also because this project is small-sized and therefore has 2 simple screens that 
  holds Bi-Directional-Flow between the UI and the Model layer but for bigger project I would recommend MVVM architecture since it has a proper separation of concerns 
  between the presentation layer and the business logic.



> ### Possible performance optimizations

> 1. **Composite Disposables** - Since RxJava/Android is not lifecycle aware, its up to me to dispose/release resources to avoid any unexpected errors/memory 
   leaks with every RxMethodCall. For smaller projects, the lifecycle events can be managed but as the project expands, it would greatly improve code
   maintenance and readability if we have a class which automatically dispose our disposables. I would create a new class AutoDisposable which extends LifecycleObserver(Android Architecture Components)
   this will have methods onBind(lifecycle), addDisposable(disposable) and onDestroy(). AutoDisposable binds to our views in the onCreate method and has a CompositeDisposable where all 
   disposables within the project will then be added, that will be disposed when the lifecycle event onDestroy() ie. Activity is destroyed.
   
   
> 2. **WeakReference** - Using a WeakReference to hold reference to the View in the Presenter, so when the View is destroyed, the WeakReference will not hold reference to it any more,
   in order to avoid memory leaks.
   
   
> 3. Leverage on a more modular architecture approach to increase build time.
   



> ### Could be done better

> 1. **Use Espresso IdlingResource** - Some of my larger test classes contains a Thread.sleep() statement to simulate/wait for network calls which is not ideal because
   * If time needed to fetch the network is much faster, then time is wasted because of the additional time waiting.
   * If the network so happens to be slow and get larger than the given time, then the test will fail prematurely.
     Integrating Espresso IdlingResource will inform us of network calls as they start and stop.


> 2. **Secure Preferences** - Encrypting and saving security/access tokens inside a secure preferences instead of declaring a string variable.


> 3. **Show a loading view** to inform the user the image is being loaded into the ImageView because sometimes images might be large in size and even though 
   Glide does a better job of loading and caching the images, they might take a while to load for the first time. It'll improve the user experience by showing
   a progress bar to the user.
   
> 4. Show concise server status messages.