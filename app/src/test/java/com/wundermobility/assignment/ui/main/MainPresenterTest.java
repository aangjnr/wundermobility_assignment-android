package com.wundermobility.assignment.ui.main;

import com.wundermobility.assignment.data.network.NetworkService;
import com.wundermobility.assignment.data.network.model.CarResponse;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Single;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class MainPresenterTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private NetworkService networkService;
    @Mock
    private MainContract.View view;

    @Test
    public void getCars_FromTheServer() {
        List<CarResponse> carResponseList = new ArrayList<>();
        CarResponse carResponse = new CarResponse();
        carResponse.setCarId(1);
        carResponseList.add(carResponse);

        Mockito.when(networkService.getCars())
                .thenReturn(Single.just(carResponseList));
        List<CarResponse> carsDataFromMockServer = networkService.getCars().blockingGet();
        assert (carsDataFromMockServer.size() > 0);
        assert (carsDataFromMockServer.get(0).mapToDomainModel().getId() == 1);
    }

    @Test
    public void attach_View() {
        assertNotNull(view);
    }
}