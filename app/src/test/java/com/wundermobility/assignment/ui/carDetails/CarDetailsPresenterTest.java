package com.wundermobility.assignment.ui.carDetails;

import com.wundermobility.assignment.data.network.NetworkService;
import com.wundermobility.assignment.data.network.model.CarDetailsResponse;
import com.wundermobility.assignment.data.network.model.ReservationResponse;
import com.wundermobility.assignment.data.domain.model.CarDetail;
import com.wundermobility.assignment.data.domain.model.Reservation;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import io.reactivex.Single;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class CarDetailsPresenterTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private NetworkService networkService;
    @Mock
    private CarDetailsContract.View view;

    @Test
    public void getCarDetails_FromTheServer() {
        CarDetailsResponse carDetailsResponse = new CarDetailsResponse();
        carDetailsResponse.setCarId(1);
        when(networkService.getCarDetails(1))
                .thenReturn(Single.just(carDetailsResponse));
        CarDetail carDetail = networkService.getCarDetails(1)
                .blockingGet()
                .mapToDomainModel();
        assert (carDetail.getCarId() == carDetailsResponse.getCarId());
    }

    @Test
    public void attach() {
        assertNotNull(view);
    }

    @Test
    public void rentACar_Request() {
        ReservationResponse reservationResponse = new ReservationResponse();
        reservationResponse.setReservationId(10);
        reservationResponse.setCarId(1);

        when(networkService.makeReservation("", null))
                .thenReturn(Single.just(reservationResponse));

        ReservationResponse reservationResponseFromServer = networkService.makeReservation("", null).blockingGet();
        Reservation reservation = reservationResponse.mapToDomainModel();

        assert (reservationResponseFromServer.getCarId() == reservation.getCarId());
    }
}