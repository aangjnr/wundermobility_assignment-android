package com.wundermobility.assignment.ui.carDetails;

import android.content.Intent;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import com.wundermobility.assignment.R;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.espresso.Espresso.onView;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class CarDetailsActivityTest {
    @Rule
    public ActivityTestRule<CarDetailsActivity> mActivityTestRule = new ActivityTestRule<>(CarDetailsActivity.class);

    @Before
    public void setUp() {
        mActivityTestRule.launchActivity(new Intent().putExtra("carId", 1));
    }

    @Test
    public void setUpViews_CheckViewIds() {
        assertThat(R.id.car_image_view, notNullValue());
        assertThat(R.id.recycler_view, notNullValue());
        assertThat(R.id.quick_rent_button, notNullValue());
    }

    @Test
    public void toggleQuickRentButton_showDialogWhenSuccessful() {
        //Replace with Idling Resources
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView((withId(R.id.quick_rent_button))).check(matches(withText("QUICK RENT")));

        onView((withId(R.id.quick_rent_button))).perform(click());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText("Success!")).check(matches(isDisplayed()));
        onView(withId(android.R.id.button1)).perform(click());
    }
}