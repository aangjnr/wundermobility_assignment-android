package com.wundermobility.assignment.ui.main;

import com.wundermobility.assignment.data.network.NetworkService;
import com.wundermobility.assignment.data.network.model.CarResponse;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableSingleObserver;

public class MainPresenter implements MainContract.Presenter {
    private NetworkService networkService;
    private MainContract.View view;

    @Inject
    MainPresenter(NetworkService mNetworkService) {
        this.networkService = mNetworkService;
    }

    @Override
    public void getCars() {
        view.showLoadingDialog("Getting data, Please wait...");
        networkService.getCars()
                .doFinally(() -> view.hideLoading())
                .subscribe(new DisposableSingleObserver<List<CarResponse>>() {
                    @Override
                    public void onSuccess(List<CarResponse> carResponseList) {
                        Observable.fromIterable(carResponseList)
                                .map(CarResponse::mapToDomainModel).toList()
                                .subscribe(cars -> view.showCarsOnMap(cars)).dispose();
                    }
                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }
                });
    }

    @Override
    public void attach(MainContract.View mView) {
        this.view = mView;
    }
}
