package com.wundermobility.assignment.ui.main;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.model.LatLngBounds;
import com.wundermobility.assignment.data.domain.model.Car;
import com.wundermobility.assignment.ui.base.BaseContract;

import java.util.List;

public interface MainContract {
    interface Presenter extends BaseContract.Presenter<View> {
        void getCars();
    }

    interface View extends BaseContract.View {
        void showCarsOnMap(List<Car> carList);
        void showCurrentLocation();
        void setCameraBounds(LatLngBounds.Builder builder);
        void moveCamera(CameraUpdate cameraUpdate);
        void goToCarDetailsActivity(int carId);
    }
}
