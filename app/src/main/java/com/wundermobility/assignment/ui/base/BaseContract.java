package com.wundermobility.assignment.ui.base;

import android.content.DialogInterface;

import androidx.annotation.Nullable;

public interface BaseContract {
    interface Presenter<T> {
        void attach(T view);
    }

    interface View {
        void showLoadingDialog(String message);
        void hideLoading();
        void showDialogMessage(String title, String message, boolean cancelable, String positiveText, @Nullable DialogInterface.OnClickListener onClickListener);
        void showError(String message, @Nullable android.view.View.OnClickListener onClickListener);
        void showError(String message);
        boolean isInternetAvailable();
        void setUpViews();
    }
}
