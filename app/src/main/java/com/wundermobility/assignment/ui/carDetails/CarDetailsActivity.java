package com.wundermobility.assignment.ui.carDetails;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.jakewharton.rxbinding3.view.RxView;
import com.wundermobility.assignment.R;
import com.wundermobility.assignment.data.domain.model.CarDetail;
import com.wundermobility.assignment.data.domain.model.Reservation;
import com.wundermobility.assignment.data.domain.model.TableData;
import com.wundermobility.assignment.ui.base.BaseActivity;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class CarDetailsActivity extends BaseActivity implements CarDetailsContract.View {
    @Inject
    CarDetailsPresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.quick_rent_button)
    Button quickRentButton;
    @BindView(R.id.car_image_view)
    ImageView carImageView;

    int carId;
    Disposable disposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_details);

        ButterKnife.bind(this);
        presenter.attach(this);
        setUpViews();

        carId = getIntent().getIntExtra("carId", -1);
        fetchCarDetails();
    }

    @Override
    public void showCarDetails(CarDetail carDetails) {
        Glide.with(this).load(carDetails.getVehicleTypeImageUrl()).into(carImageView);

        List<TableData> data = new ArrayList<>();
        data.add(new TableData(0, "FIELD", "VALUE"));

        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(carDetails));
            Observable.range(0, jsonObject.length())
                    .doOnNext(i -> {
                        String key = jsonObject.names().getString(i);
                        data.add(new TableData(i + 1, key, jsonObject.getString(key)));
                    }).doFinally(() -> {
                recyclerView.setHasFixedSize(true);
                GridLayoutManager manager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(manager);
                CarDetailsRecyclerAdapter adapter = new CarDetailsRecyclerAdapter(data, ContextCompat.getColor(this, R.color.colorAccentLighter));
                recyclerView.setAdapter(adapter);
            })
                    .subscribe()
                    .dispose();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showReservationInfo(Reservation reservation) {
        String messageBuilder = "Your reservation has been completed successfully!\nSee details below:" +
                "\nLicence Plate : " + reservation.getLicencePlate() +
                "\nStart Address : " + reservation.getStartAddress() +
                "\nFuel Card Pin : " + reservation.getFuelCardPin();
        showDialogMessage("Success!", messageBuilder, true, "Ok", (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void showError(String message) {
        super.showError(message, getRetryClickListener());
    }

    @Override
    public void setUpViews() {
        toolbar.setTitle("Car Details");
        setSupportActionBar(toolbar);

        disposable = RxView.clicks(quickRentButton)
                .subscribe(aVoid -> {
                    if (isInternetAvailable())
                        presenter.rentACar(carId);
                    else
                        showError(getResources().getString(R.string.no_internet_connection), null);
                });
        super.setUpViews();
    }


    void fetchCarDetails() {
        if (isInternetAvailable())
            presenter.getCarDetails(carId);
        else
            showError(getResources().getString(R.string.no_internet_connection), getRetryClickListener());
    }

    private View.OnClickListener getRetryClickListener() {
        return v -> fetchCarDetails();
    }

    @Override
    public void toggleQuickRentButton(boolean shouldEnable) {
        quickRentButton.setEnabled(shouldEnable);
    }

    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }
}
