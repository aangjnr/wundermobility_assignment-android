package com.wundermobility.assignment.ui.carDetails;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.recyclerview.widget.RecyclerView;
import com.wundermobility.assignment.R;
import com.wundermobility.assignment.data.domain.model.TableData;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CarDetailsRecyclerAdapter extends RecyclerView.Adapter<CarDetailsRecyclerAdapter.ViewHolder> {
    private List<TableData> list;
    private int backgroundColor;

    CarDetailsRecyclerAdapter(List<TableData> list, @ColorInt int backgroundColor) {
        this.list = list;
        this.backgroundColor = backgroundColor;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_detail_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.firstColumn.setText(list.get(position).getFirstColumn());
        holder.secondColumn.setText(list.get(position).getSecondColumn());

        if (position == 0) {
            setTitleStyling(holder.firstColumn);
            setTitleStyling(holder.secondColumn);
        }

        if (position % 2 == 0)
            holder.itemView.setBackgroundColor(backgroundColor);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void setTitleStyling(TextView textView) {
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextSize(16f);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.first_column)
        TextView firstColumn;
        @BindView(R.id.second_column)
        TextView secondColumn;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}