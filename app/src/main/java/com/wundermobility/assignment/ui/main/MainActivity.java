package com.wundermobility.assignment.ui.main;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wundermobility.assignment.R;
import com.wundermobility.assignment.data.domain.model.Car;
import com.wundermobility.assignment.ui.base.BaseActivity;
import com.wundermobility.assignment.ui.carDetails.CarDetailsActivity;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import ru.alexbykov.nopermission.PermissionHelper;

public class MainActivity extends BaseActivity implements MainContract.View, OnMapReadyCallback {

    @Inject
    MainPresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    List<Marker> markers = new ArrayList<>();
    boolean isInfoWindowShown = false;
    PermissionHelper permissionHelper;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter.attach(this);
        setUpViews();
    }

    @Override
    public void onMapReady(GoogleMap mGoogleMap) {
        googleMap = mGoogleMap;
        checkLocationPermission();
    }

    @Override
    public void showCarsOnMap(List<Car> cars) {
        //Cars without location or lat, lon == 0 were filtered out of the LatLngBounds.Builder on purpose
        //This is so that the Camera can zoom within the bounds because
        // zooming out of the map hides some markers behind others making it difficult to click on a marker

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        Observable.fromIterable(cars)
                .doOnNext(this::addMarker)
                .filter(car -> car.getLat() != 0.0 && car.getLon() != 0.0)
                .filter(car -> car.getTitle() != null && !car.getTitle().isEmpty())
                .doOnNext(car -> builder.include(new LatLng(car.getLat(), car.getLon())))
                .doFinally(() -> setCameraBounds(builder))
                .subscribe().dispose();

        addMarkerClickListener();
    }


    void addMarker(Car car) {
        MarkerOptions markerOptions = new MarkerOptions()
                .draggable(false)
                .snippet(String.valueOf(car.getId()))
                .title(car.getTitle())
                .position(new LatLng(car.getLat(), car.getLon()));
        markers.add(googleMap.addMarker(markerOptions));
    }

    void addMarkerClickListener() {
        googleMap.setOnMarkerClickListener(marker -> {
            if (isInfoWindowShown) {
                goToCarDetailsActivity(Integer.parseInt(marker.getSnippet()));
            } else {
                marker.showInfoWindow();
                hideMarkers(marker);
                moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 17));
                isInfoWindowShown = true;
            }
            return true;
        });
    }

    void hideMarkers(Marker markerToShow) {
        Observable.fromIterable(markers)
                .filter(marker -> !marker.getSnippet().equals(markerToShow.getSnippet()))
                .doOnNext(marker -> marker.setVisible(false))
                .subscribe().dispose();
    }

    @Override
    public void setUpViews() {
        toolbar.setTitle("Map View");
        setSupportActionBar(toolbar);
        super.setUpViews();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null)
            mapFragment.getMapAsync(this);
        else
            showError(string(R.string.map_fragment_not_found), null);
    }

    void checkLocationPermission() {
        permissionHelper = new PermissionHelper(this).check(Manifest.permission.ACCESS_FINE_LOCATION)
                .onSuccess(this::showCurrentLocation)
                .onDenied(() -> showDialogMessage(string(R.string.location_permission),
                        string(R.string.location_permission_rationale),
                        true,
                        string(R.string.check_permission), (dialog, which) -> {
                            checkLocationPermission();
                            dialog.dismiss();
                        })
                );
        permissionHelper.run();
    }

    void getCarList() {
        if (isInternetAvailable())
            presenter.getCars();
        else
            showError(string(R.string.no_internet_connection), getRetryClickListener());
    }

    @Override
    public void goToCarDetailsActivity(int carId) {
        startActivity(new Intent(this, CarDetailsActivity.class)
                .putExtra("carId", carId));
    }

    @Override
    public void showCurrentLocation() {
        googleMap.setMyLocationEnabled(true);
        getCarList();
    }

    public void showError(String message){
        super.showError(message, getRetryClickListener());
    }

    @Override
    public void moveCamera(CameraUpdate cameraUpdate) {
        googleMap.animateCamera(cameraUpdate);
    }

    @Override
    public void setCameraBounds(LatLngBounds.Builder builder) {
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20); // offset from edges of the map 10% of screen

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        moveCamera(cameraUpdate);
    }

    @Override
    public void showError(String message, @Nullable View.OnClickListener onClickListener) {
        super.showError(message, getRetryClickListener());
    }

    private View.OnClickListener getRetryClickListener() {
        return v -> getCarList();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
