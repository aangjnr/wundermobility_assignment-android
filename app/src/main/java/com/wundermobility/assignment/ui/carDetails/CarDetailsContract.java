package com.wundermobility.assignment.ui.carDetails;

import com.wundermobility.assignment.data.domain.model.CarDetail;
import com.wundermobility.assignment.data.domain.model.Reservation;
import com.wundermobility.assignment.ui.base.BaseContract;

public interface CarDetailsContract {
    interface Presenter extends BaseContract.Presenter<View> {
        void getCarDetails(int carId);
        void rentACar(int carId);
    }

    interface View extends BaseContract.View {
        void showCarDetails(CarDetail carDetails);
        void showReservationInfo(Reservation reservation);
        void toggleQuickRentButton(boolean shouldEnable);
    }
}
