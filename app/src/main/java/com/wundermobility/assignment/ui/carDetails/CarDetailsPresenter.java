package com.wundermobility.assignment.ui.carDetails;

import com.google.gson.JsonObject;
import com.wundermobility.assignment.BuildConfig;
import com.wundermobility.assignment.data.network.NetworkService;
import com.wundermobility.assignment.data.network.model.CarDetailsResponse;
import com.wundermobility.assignment.data.network.model.ReservationResponse;
import com.wundermobility.assignment.di.module.AppSubModule;
import javax.inject.Inject;
import io.reactivex.observers.DisposableSingleObserver;

public class CarDetailsPresenter implements CarDetailsContract.Presenter {

    private NetworkService networkService;
    private CarDetailsContract.View view;

    @Inject
    CarDetailsPresenter(NetworkService mNetworkService) {
        this.networkService = mNetworkService;
    }

    @Override
    public void getCarDetails(int carId) {
        view.showLoadingDialog("Getting car info, Please wait...");
        networkService.getCarDetails(carId)
                .doFinally(() -> view.hideLoading())
                .subscribe(new DisposableSingleObserver<CarDetailsResponse>() {
                    @Override
                    public void onSuccess(CarDetailsResponse carDetailsResponse) {
                        view.toggleQuickRentButton(true);
                        view.showCarDetails(carDetailsResponse.mapToDomainModel());
                    }
                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                        view.toggleQuickRentButton(false);
                    }
                });
    }

    @Override
    public void rentACar(int carId) {
        view.showLoadingDialog("Issuing rent request, Please wait...");

        //Since Dagger is used as DI in this project, we need a way to assign some values in the auto generated files at runtime
        //isAuthorizationRequired = true ensures dagger adds the Authorization header to the request
        //Before issuing since its a protected endpoint
        AppSubModule.isAuthorizationRequired = true;

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("carId", carId);
        networkService.makeReservation(BuildConfig.QUICK_RENT_URL, jsonObject)
                .doFinally(() -> view.hideLoading())
                .subscribe(new DisposableSingleObserver<ReservationResponse>() {
                    @Override
                    public void onSuccess(ReservationResponse reservationResponse) {
                        view.showReservationInfo(reservationResponse.mapToDomainModel());
                    }
                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage(), null);
                    }
                });
    }

    @Override
    public void attach(CarDetailsContract.View mView) {
        this.view = mView;
    }
}
