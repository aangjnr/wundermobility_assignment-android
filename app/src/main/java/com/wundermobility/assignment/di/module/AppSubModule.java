package com.wundermobility.assignment.di.module;

import com.wundermobility.assignment.BuildConfig;
import com.wundermobility.assignment.data.network.ApiInterface;
import com.wundermobility.assignment.data.network.NetworkService;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = AppModule.class)
public class AppSubModule {

    public static boolean isAuthorizationRequired = false;

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        if(BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(interceptor);
        }
        httpClient.addInterceptor(chain -> {
            Request.Builder requestBuilder = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json");
            if (isAuthorizationRequired)
                requestBuilder.addHeader("Authorization", "Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa");
            return chain.proceed(requestBuilder.build());
        });
        return httpClient.build();
    }

    @Singleton
    @Provides
    Retrofit providesRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    @Singleton
    @Provides
    ApiInterface providesApiInterface(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }

    @Singleton
    @Provides
    NetworkService providesNetworkService(ApiInterface apiInterface) {
        return new NetworkService(apiInterface);
    }
}
