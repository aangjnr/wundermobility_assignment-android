package com.wundermobility.assignment.di.module;

import com.wundermobility.assignment.ui.carDetails.CarDetailsActivity;
import com.wundermobility.assignment.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityContributorModule {
    @ContributesAndroidInjector
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector
    abstract CarDetailsActivity contributesCarDetailsActivity();

}
