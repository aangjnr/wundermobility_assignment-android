package com.wundermobility.assignment.data.network;

import com.google.gson.JsonObject;
import com.wundermobility.assignment.data.network.model.CarDetailsResponse;
import com.wundermobility.assignment.data.network.model.CarResponse;
import com.wundermobility.assignment.data.network.model.ReservationResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface ApiInterface {
    @GET("cars.json")
    Single<List<CarResponse>> getCarList();

    @GET("cars/{carId}")
    Single<CarDetailsResponse> getCarData(@Path("carId") int carId);

    @POST
    Single<ReservationResponse> quickRent(@Url String reservationUrl, @Body JsonObject body);
}
