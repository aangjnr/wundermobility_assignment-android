package com.wundermobility.assignment.data.network.model;

abstract class BaseResponse {
    int carId;
    String title;
    double lat;
    double lon;
    String licencePlate;
    int fuelLevel;
    int vehicleStateId;
    int vehicleTypeId;
    String pricingTime;
    String pricingParking;
    int reservationState;
    boolean isClean;
    boolean isDamaged;
    String address;
    String zipCode;
    String city;
    int locationId;

    BaseResponse() {
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }
}
