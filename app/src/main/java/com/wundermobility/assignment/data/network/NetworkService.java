package com.wundermobility.assignment.data.network;

import com.google.gson.JsonObject;
import com.wundermobility.assignment.data.network.model.CarDetailsResponse;
import com.wundermobility.assignment.data.network.model.CarResponse;
import com.wundermobility.assignment.data.network.model.ReservationResponse;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class NetworkService {

    private ApiInterface apiInterface;

    @Inject
    public NetworkService(ApiInterface mApiInterface) {
        this.apiInterface = mApiInterface;
    }

    public Single<List<CarResponse>> getCars() {
        return apiInterface.getCarList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<CarDetailsResponse> getCarDetails(int carId) {
        return apiInterface.getCarData(carId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<ReservationResponse> makeReservation(String reservationUrl, JsonObject body) {
        return apiInterface.quickRent(reservationUrl, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
