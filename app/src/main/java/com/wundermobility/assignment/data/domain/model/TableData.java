package com.wundermobility.assignment.data.domain.model;

public class TableData {
    private int id;
    private String firstColumn;
    private String secondColumn;


    public TableData(int mId, String mFirstColumn, String mSecondColumn) {
        this.id = mId;
        this.firstColumn = mFirstColumn;
        this.secondColumn = mSecondColumn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstColumn() {
        return firstColumn;
    }

    public String getSecondColumn() {
        return secondColumn;
    }

}
