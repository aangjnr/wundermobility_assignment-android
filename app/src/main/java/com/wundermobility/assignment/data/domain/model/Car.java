package com.wundermobility.assignment.data.domain.model;

public class Car {
    private int id;
    private String title;
    private String licencePlate;
    private double lat;
    private double lon;


    public Car(int carId, String carTitle, String mLicencePlate, double carLat, double carLon) {
        this.id = carId;
        this.title = carTitle;
        this.licencePlate = mLicencePlate;
        this.lat = carLat;
        this.lon = carLon;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }
}
