package com.wundermobility.assignment.data.domain.model;

public class Reservation {
    private int id;
    private int cost;
    private String licencePlate;
    private String startAddress;
    private int userId;
    private int carId;
    private String damageDescription;
    private int drivenDistance;
    private String fuelCardPin;
    private int startTime;
    private int endTime;

    public Reservation(int mReservationId, int mCarId, int mCost, String mLicencePlate,
                       String mStartAddress, int mUserId, String mDamageDescription,
                       int mDrivenDistance, String mFuelCardPin, int mStartTime, int mEndTime) {
        this.id = mReservationId;
        this.carId = mCarId;

        this.cost = mCost;
        this.licencePlate = mLicencePlate;
        this.startAddress = mStartAddress;
        this.userId = mUserId;
        this.damageDescription = mDamageDescription;
        this.drivenDistance = mDrivenDistance;
        this.fuelCardPin = mFuelCardPin;
        this.startTime = mStartTime;
        this.endTime = mEndTime;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public String getFuelCardPin() {
        return fuelCardPin;
    }

}
