package com.wundermobility.assignment.data.network.model;

class NetworkResult {
    interface DomainMapper<T> {
        T mapToDomainModel();
    }
}
