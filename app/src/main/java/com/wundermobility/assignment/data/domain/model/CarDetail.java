package com.wundermobility.assignment.data.domain.model;

public class CarDetail {
    private int carId;
    private String title;
    private String licencePlate;
    private int fuelLevel;
    private String pricingTime;
    private String pricingParking;
    private String address;
    private String zipCode;
    private String city;
    private String damageDescription;
    private String vehicleTypeImageUrl;


    public CarDetail() {
    }

    public CarDetail(int mCarId, String mTitle, String mLicencePlate, int mFuelLevel, String mPricingTime, String mPricingParking, String mAddress,
                     String mZipCode, String mCity, String mDamageDescription, String mVehicleUrl) {
        this.carId = mCarId;
        this.title = mTitle;
        this.licencePlate = mLicencePlate;
        this.fuelLevel = mFuelLevel;
        this.pricingTime = mPricingTime;
        this.pricingParking = mPricingParking;
        this.address = mAddress;
        this.zipCode = mZipCode;
        this.city = mCity;
        this.damageDescription = mDamageDescription;
        this.vehicleTypeImageUrl = mVehicleUrl;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getVehicleTypeImageUrl() {
        return vehicleTypeImageUrl;
    }

}
