package com.wundermobility.assignment.data.network.model;

import com.wundermobility.assignment.data.domain.model.Reservation;

public class ReservationResponse implements NetworkResult.DomainMapper<Reservation> {
    private int reservationId;
    private int carId;
    private int cost;
    private String licencePlate;
    private String startAddress;
    private boolean isParkModeEnabled;
    private int userId;
    private String damageDescription;
    private int drivenDistance;
    private String fuelCardPin;
    private int startTime;
    private int endTime;

    public ReservationResponse() {
    }

    @Override
    public Reservation mapToDomainModel() {
        return new Reservation(reservationId, carId, cost, licencePlate, startAddress, userId,
                damageDescription, drivenDistance, fuelCardPin, startTime, endTime);
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }
}
