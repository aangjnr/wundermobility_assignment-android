package com.wundermobility.assignment.data.network.model;

import com.wundermobility.assignment.data.domain.model.CarDetail;

public class CarDetailsResponse extends BaseResponse implements NetworkResult.DomainMapper<CarDetail> {
    private String hardwareId;
    private boolean isActivatedByHardware;
    private String damageDescription;
    private String vehicleTypeImageUrl;


    public CarDetailsResponse() {
    }


    @Override
    public CarDetail mapToDomainModel() {
        return new CarDetail(carId, title, licencePlate, fuelLevel, pricingTime, pricingParking,
                address, zipCode, city, damageDescription, vehicleTypeImageUrl);
    }
}
