package com.wundermobility.assignment.data.network.model;

import com.wundermobility.assignment.data.domain.model.Car;

public class CarResponse extends BaseResponse implements NetworkResult.DomainMapper<Car> {
    private String distance;

    public CarResponse() {
    }

    @Override
    public Car mapToDomainModel() {
        return new Car(carId, title, licencePlate, lat, lon);
    }
}
